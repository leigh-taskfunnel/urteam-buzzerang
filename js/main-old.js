$( document ).ready(function() {
	setInterval(function() {
		if($('#hlh-logo').length < 1) {
			// set elements needed to render
			$('<img id="hlh-logo" src="https://firebasestorage.googleapis.com/v0/b/highlevel-backend.appspot.com/o/companyPhotos%2Fvi4d3Em23oWHAPxhya3I.png?alt=media&token=c08d5d07-fb84-4e7e-a88a-79f056edf41c" width="350" height="66">').insertBefore('.hl_header--picker');
			$('#nav-links').insertAfter('header.hl_header .container-fluid');
		}

		if ($('#dashboard-original').length < 1) {
			if(window.location.href.indexOf("dashboard") > -1) {
				$('#dashboard').attr('id', 'dashboard-original');
        		$('.pending-task-value').css('background-color', 'transparent!important');
			}
        }

        if ($('#chat-widget').length < 1) {
			if(window.location.href.indexOf("chat_widget_settings") > -1) {
				$('section').attr('id', 'chat-widget');
			}
        }

        if($('#hl_header--help-icon').attr('style').indexOf('background: var(--brand-accent)')) {
        	$('#hl_header--help-icon').attr('style', 'background: var(--brand-accent) !important;');
        	$('#hl_header--help-icon i').attr('style', 'color: var(--brand-primary) !important;');
        }

        $("a[href='#nav-marketing-collapse']").attr('id', 'nav-marketing-col-menubar');

        $('#nav-marketing-col-menubar').click(function() {
            $('#nav-marketing-collapse').attr('style', 'height: 0px!important;');

        });

		console.log("function running...");
	}, 500);

});